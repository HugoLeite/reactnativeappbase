import React, { Menu } from 'react'
import Home from './containers/home/home'
import Diagnostico from './containers/diagnostico/diagnostico'
import DiagnosticoGastos from './containers/diagnostico/diagnosticoGastos'
import DiagnosticoGastosDois from './containers/diagnostico/diagnosticoGastosDois'
import Aplicacoes from './containers/diagnostico/aplicacoes'
import { createDrawerNavigator, createStackNavigator } from 'react-navigation'

const HomeStack = createStackNavigator({ Home },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#90c74a',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  })

const DiagnosticoStack = createStackNavigator({
  Diagnostico: {
    screen: Diagnostico 
  },
  DiagnosticoGastos: {
    screen: DiagnosticoGastos
  },
  DiagnosticoGastosDois: {
    screen: DiagnosticoGastosDois
  },
  Aplicacoes: {
    screen: Aplicacoes
  }
},
{
  initialRouteName: 'Diagnostico',
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#90c74a',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
})

const DrawerNavigation = createDrawerNavigator({
  Home: {
    screen: HomeStack
  },
  Diagnostico: {
    screen: DiagnosticoStack
  }
})

export default class App extends React.Component {
  render() {
    return (
      <DrawerNavigation />
    );
  }
}
