import React from 'react';
import { View, StyleSheet, TextInput, Text } from 'react-native'

const gastoInput = (props) => (
    <View style={{flexDirection: "row", marginBottom: 15}}>
        <Text style={{flex: 1, paddingTop: 5}}>{props.descricao}</Text>
        <View style={styles.viewInput}>
            <TextInput 
                style={styles.textInput}
                value={props.value} 
                placeholder="R$1000,00"
                onChangeText={props.changeValue}
            ></TextInput>
        </View>
    </View>
)

const styles = StyleSheet.create({
    viewInput: {
        borderBottomColor: "#ccc",
        borderBottomWidth: 2,
        flex: 0.4
    },
    textInput: {
        marginBottom: -10,
        marginTop: -10
    }
  })

export default gastoInput