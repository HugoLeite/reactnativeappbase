import React from 'react';
import { View, StyleSheet, TextInput, Picker } from 'react-native';

const rendaInput = (props) => (
    <View>
        <Picker
            selectedValue={props.type}
            style={styles.picker}
            onValueChange={props.changeType}>
            <Picker.Item label="Selecione o tipo de aplicação" value="" />
            <Picker.Item label="Poupança" value="poupanca" />
            <Picker.Item label="CDB" value="cdb" />
            <Picker.Item label="Ações" value="acoes" />
        </Picker>
        <View style={styles.viewInput}>
            <TextInput 
                style={styles.textInput}
                value={props.value} 
                placeholder="R$1000,00"
                onChangeText={props.changeValue}
            ></TextInput>
        </View>
    </View>
)

const styles = StyleSheet.create({
    picker: {
        height: 50,
        width: '100%',
        marginBottom: -15
    },
    viewInput: {
        borderBottomColor: "#ccc",
        borderBottomWidth: 2,
        marginBottom: 10
    },
    textInput: {
    }
  })

export default rendaInput