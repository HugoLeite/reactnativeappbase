import React from 'react';
import { View, Text } from 'react-native';

const rendaText = (props) => (
    <View style={{flexDirection: "row"}}>
        <Text style={{flex: 1}}>{props.renda}:</Text>
        <Text style={{flex: 1, textAlign: "right"}}>R${props.valor}</Text>
    </View>
)

export default rendaText