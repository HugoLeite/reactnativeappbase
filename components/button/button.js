import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import Aux from '../../hoc/Aux/aux'

export default class Button extends React.Component {
    render() {
        let direction, button
        switch (this.props.iconPosition) {
            case 'top':
                direction = "column"
                button = <Aux>
                    <Icon style={styles[direction]} name={this.props.icon} size={20} />
                    <Text style={{flex: 1}}>{this.props.title}</Text>
                </Aux>
                break;
            case 'bottom':
                direction = "column"
                button = <Aux>
                    <Text style={{flex: 1}}>{this.props.title}</Text>
                    <Icon style={styles[direction]} name={this.props.icon} size={20} />
                </Aux>
                break;
            case 'left':
                direction = "row"
                button = <Aux>
                    <Icon style={styles[direction]} name={this.props.icon} size={20} />
                    <Text style={{flex: 1}}>{this.props.title}</Text>
                </Aux>
                break;
            case 'right':
                direction = "row"
                button = <Aux>
                    <Text style={{flex: 1}}>{this.props.title}</Text>
                    <Icon style={styles[direction]} name={this.props.icon} size={20} />
                </Aux>
                break;
            default:
                direction = "column"
                button = <Aux>
                    <Icon style={styles[direction]} name={this.props.icon} size={20} />
                    <Text style={{flex: 1}}>{this.props.title}</Text>
                </Aux>
                break;
        }
        
        return (
            <TouchableOpacity onPress={() => this.props.onPress()}
             style={[styles.button, {
                    backgroundColor: this.props.buttonStyles.color,
                    width: this.props.buttonStyles.width,
                    flexDirection: direction
                }]}>
                {button}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: "center",
        shadowColor: '#cccccc',
        shadowOffset: {width: 10, height: 10},
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 10,
        padding: 5
    },
    row: {
        flex: 0.25
    },
    column: {
        flex: 1
    }
    
  })