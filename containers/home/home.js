import React from 'react';
import { View, StyleSheet } from 'react-native'
import { DrawerActions } from 'react-navigation'
import Button from '../../components/button/button'
import Aux from '../../hoc/Aux/aux'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class Home extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Início',
            headerRight: (
            <Icon 
                name="bars" 
                size={25} 
                style={{color: "#fff", paddingRight: 15}}
                onPress={() => {navigation.dispatch(DrawerActions.openDrawer())}} />
            )
        }
      }
    render() {
        return (
            <Aux>
                <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'stretch',
                    padding: 15
                }}>
                    <View style={styles.containerButton}>
                        <Button
                            raised
                            icon='cog'
                            title='Configuração'
                            onPress={ () => {}}
                            buttonStyles={{
                                color:'#ccc',
                                width:150
                            }}/>
                        <Button
                            raised
                            icon='comment'
                            title='Ajuda'
                            onPress={ () => {}}
                            buttonStyles={{
                                color:'#ccc',
                                width:150
                            }}/>
                    </View>
                    <View style={styles.containerButton}>
                        <Button
                            raised
                            icon='dollar'
                            title='Recebimento'
                            onPress={ () => {}}
                            buttonStyles={{
                                color:'#ccc',
                                width:150
                            }}/>
                        <Button
                            raised
                            icon='dollar'
                            title='Gastos'
                            onPress={ () => {}}
                            buttonStyles={{
                                color:'#ccc',
                                width:150
                            }}/>
                    </View>
                    <View style={styles.containerButton}>
                        <Button
                            raised
                            icon='clipboard'
                            title='Diagnostico Financeiro'
                            buttonStyles={{
                                color:'#ccc',
                                width:'100%'
                            }}
                            onPress={ () => this.props.navigation.navigate('Diagnostico')}/> 
                    </View>
                </View>
            </Aux>
        );
    }
}

const styles = StyleSheet.create({
    containerButton: {
        flexDirection: 'row', 
        height: 60, 
        justifyContent: 'space-between', 
        marginBottom: 20
    }
  })