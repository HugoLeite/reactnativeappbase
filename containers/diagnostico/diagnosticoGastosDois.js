import React from 'react';
import { View, StyleSheet, Text, Button, TextInput} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DrawerActions } from 'react-navigation'
import Accordion from 'react-native-collapsible/Accordion'
import Aux from '../../hoc/Aux/aux'

const SECTIONS = [
    {
      title: 'Habitação',
      placeholder: 'Descrição (aluguel, condomínio...)'
    },
    {
      title: 'Transporte',
      placeholder: 'Descrição (Ônibus, carro, moto...)'
    },
    {
      title: 'Empréstimo',
      placeholder: 'Descrição'
    },
    {
      title: 'Outros',
      placeholder: 'Descrição'
    }
  ];
export default class Diagnostico extends React.Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Gastos',
            headerRight: (
            <Icon 
                name="bars" 
                size={25} 
                style={{color: "#fff", paddingRight: 15}}
                onPress={() => {navigation.dispatch(DrawerActions.openDrawer())}} />
            )
        }
      }

      state = {
        activeSections: []
      };
    
      _renderHeader = section => {
        return (
          <View style={styles.header}>
            <Text style={styles.headerText}>{section.title}</Text>
          </View>
        );
      };
    
      _renderContent = section => {
        return (
          <View style={styles.content}>
            <View style={{flexDirection: "row"}}>
                <View style={[styles.viewInput, {flex: 1}]}>
                    <TextInput 
                        style={styles.textInput}
                        placeholder={section.placeholder}
                    ></TextInput>
                </View>
                <View style={[styles.viewInput, {flex: 0.5}]}>
                    <TextInput 
                        style={styles.textInput}
                        placeholder="R$1000,00"
                    ></TextInput>
                </View>
                <Icon name="plus" size={20} style={{flex: 0.2, textAlign: 'center'}} />
            </View>
            <View style={{flexDirection:"row", padding: 10, marginTop: 10}}>
              <Text style={{flex:1}}>Aluguel:</Text>
              <Text style={{flex:1, textAlign:"right"}}>R$900,00</Text>
            </View>
          </View>
        );
      };
    
      _updateSections = activeSections => {
        this.setState({ activeSections });
      };
    
      render() {
        return (
          <Aux>
            <Accordion
              sections={SECTIONS}
              activeSections={this.state.activeSections}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
            />
            <View style={{position: "absolute", bottom: 15, right: 15}}>
                <View style={{width: "100%", alignItems: "flex-end"}}>
                    <Button title="Proximo" onPress={ () => this.props.navigation.navigate('Aplicacoes')} />
                </View>
            </View>
          </Aux>
        );
      }
}

const styles = StyleSheet.create({
    content: {
        padding: 15,
        borderBottomColor: "#ccc",
        borderBottomWidth: 1,
        backgroundColor: "#ddd"
    },
    header: {
      borderBottomColor: "#ccc",
      borderBottomWidth: 1,
      padding: 20
    },
    headerText: {
      fontSize: 15
    },
    viewInput: {
      borderBottomColor: "#ccc",
      borderBottomWidth: 2,
      marginRight: 10
    },
    textInput: {
        marginBottom: -10,
        marginTop: -10
    }
  })