import React from 'react';
import { View, StyleSheet, Text, Button} from 'react-native'
import Aux from '../../hoc/Aux/aux'
import GastoInput from '../../components/diagnostico/gastoInput'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DrawerActions } from 'react-navigation'

export default class Diagnostico extends React.Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Gastos',
            headerRight: (
            <Icon 
                name="bars" 
                size={25} 
                style={{color: "#fff", paddingRight: 15}}
                onPress={() => {navigation.dispatch(DrawerActions.openDrawer())}} />
            )
        }
      }

    render() {
        return (
            <Aux>
                <View style={styles.container}>
                    <View style={{flex: 0.1, marginBottom: 15}}>
                        <Text>Informe seus principais gastos mensais</Text>
                    </View>
                    <View>
                        <GastoInput 
                            descricao="Alimentação"/>
                        <GastoInput 
                            descricao="Saúde"/>
                        <GastoInput
                            descricao="Educação"/>
                        <GastoInput 
                            descricao="Energia"/>
                        <GastoInput 
                            descricao="Gás"/>
                        <GastoInput 
                            descricao="Água"/>
                        <GastoInput 
                            descricao="Vestuário"/>
                        <GastoInput 
                            descricao="Lazer"/>
                    </View>
                    <View style={{position: "absolute", bottom: 15, right: 15}}>
                        <View style={{width: "100%", alignItems: "flex-end"}}>
                            <Button title="Proximo" onPress={ () => this.props.navigation.navigate('DiagnosticoGastosDois')} />
                        </View>
                    </View>
                </View>
            </Aux>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        flexDirection: "column",
        flex: 1
    }
  })