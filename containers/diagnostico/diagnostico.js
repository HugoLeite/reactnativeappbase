import React from 'react';
import { View, StyleSheet, Text, Button, SectionList } from 'react-native';
import Aux from '../../hoc/Aux/aux'
import RendaInput from '../../components/diagnostico/rendaInput'
import RendaText from '../../components/diagnostico/rendaText'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DrawerActions } from 'react-navigation'


export default class Diagnostico extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            type: "",
            value: "",
            rendas: []
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Diagnostico',
            headerRight: (
            <Icon 
                name="bars" 
                size={25} 
                style={{color: "#fff", paddingRight: 15}}
                onPress={() => {navigation.dispatch(DrawerActions.openDrawer())}} />
            )
        }
      }

    addRenda = () => {
        var rendas = this.state.rendas
        rendas.push({
            renda: this.state.type,
            valor: this.state.value
        })
        this.setState({
            type: "",
            value: "",
            rendas: rendas
        })
    }

    render() {
        return(
            <Aux>
                <View style={styles.container}>
                    <View style={{flex: 0.1}}>
                        <Text>Informe seus rendimentos</Text>
                    </View>
                    <View style={{flex: 0.5}}>
                        <RendaInput 
                            value={this.state.value}
                            type={this.state.type}
                            changeType={(itemValue) => { this.setState({type: itemValue})}}
                            changeValue={(text) => { this.setState({value: text})}}></RendaInput>
                        <Button title="Adicionar" onPress={this.addRenda} />
                    </View>
                    <View style={{flex: 1, overflow: "scroll"}}>
                        <SectionList
                            renderItem={({ item, index, section }) => <RendaText key={index} renda={item.renda} valor={item.valor}></RendaText>}
                            sections={[
                                { data: this.state.rendas}
                            ]}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{flex: 0.1}}>
                        <View style={{width: "100%", alignItems: "flex-end"}}>
                            <Button title="Proximo" onPress={ () => this.props.navigation.navigate('DiagnosticoGastos')} />
                        </View>
                    </View>
                </View>
            </Aux>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        flexDirection: "column",
        flex: 1
    }
  })